package com.cqeec.ams.controller;

import java.util.*;

import com.cqeec.ams.pojo.Account;
import com.cqeec.ams.service.Impl.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cqeec.ams.common.EasyUiTreeNodeBean;

import static javax.xml.ws.soap.AddressingFeature.ID;

@Controller
@RequestMapping("/admin")
public class AccountController {

  @Autowired
  private AccountService accountService;


	/**
	 * 新增用户数据
	 * @param account
	 * @return
	 */
	@RequestMapping("/preserve")
	@ResponseBody
	public Map<String, Object> save(Account account) {
		Map<String, Object> map = new HashMap<String, Object>();
		String Id = account.getId();
		try {
			if (Id == null ||"".equals(Id)) {
				System.out.println("插入进入");
				// 新增账户
				account.setId(UUID.randomUUID().toString().toUpperCase());
 				accountService.save(account);
				map.put("code", 0);
				map.put("message", "新增用户成功！");
			} else {
				// 更新账户
				accountService.upAccount(account);
				map.put("code", 0);
				map.put("message", "更新用户信息成功！");
			}
		} catch (Exception e) {
			map.put("code", 1);
			map.put("message", "添加或更新用户信息失败！");
			e.printStackTrace();
		}
		return map;
	}

	/**
	 * 获取表格数据
	 * @return
	 */
	@RequestMapping("/getAllAccount")
	@ResponseBody
	public List<Account> getAllAccount(){
		return accountService.getAllAccount();

	}

	@PatchMapping("/delete/{id}")
	@ResponseBody
	public Map<String,Object> delete(@RequestParam String Id) {
		Map<String, Object> map = new HashMap<String, Object>();
		System.out.println(Id);

		try {
			if (Id != null || !("".equals(Id))) {
  				//删除数据
				accountService.AccountDelete(ID);
				map.put("code", 1);
				map.put("message", "删除用户成功！");
			} else {
				map.put("code", 0);
				map.put("message", "删除信息失败！");
			}
		} catch (Exception e) {
			map.put("code", 0);
			map.put("message", "服务器忙！");
			e.printStackTrace();
		}
		return map;


	}

	@RequestMapping("/permission")
	@ResponseBody
	public List<EasyUiTreeNodeBean> getAccountPermission() {
		List<EasyUiTreeNodeBean> list = new ArrayList<EasyUiTreeNodeBean>();

		EasyUiTreeNodeBean node = new EasyUiTreeNodeBean();
		node.setId("001");
		node.setText("系统管理");
		List<EasyUiTreeNodeBean> children = new ArrayList<EasyUiTreeNodeBean>();
		EasyUiTreeNodeBean subNode = new EasyUiTreeNodeBean();
		subNode.setId("001001");
		subNode.setIconCls("icon-edit");
		subNode.setText("用户管理");
		subNode.addAttribute("url", "http://localhost:8080/main_war_exploded/Account.jsp");
		children.add(subNode);
		subNode = new EasyUiTreeNodeBean();
		subNode.setId("001002");
		subNode.setText("角色管理");
		children.add(subNode);
		node.setChildren(children);
		subNode.addAttribute("url", "../index2.jsp");
		list.add(node);

		return list;
	}

}
