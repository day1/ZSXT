package com.cqeec.ams.dao.Impl;

import com.cqeec.ams.pojo.Account;

import java.util.List;


public interface AccountMapper {
	public List<Account> findAll();

	public List<Account> findByCondition(Account account);

	public Account findById(String id);

	public Account findByAccountNameAndPassword(Account account);

	public Account findByAccountNameAndPassword2(String name, String password);

	public void insert(Account account);

	public void insertBatch(List<Account> list);

	public void update(Account account);

	public void delete(String id);
}
