package com.cqeec.ams.dao.Impl;

import com.cqeec.ams.pojo.Role;

import java.util.List;
import java.util.Map;




public interface RoleMapper {
	public List<Role> findAll();

	public List<Role> findByPage(Role role);

	public List<Role> findByPage(Map<String, Object> map);

	public List<Role> findByCondition(Role role);

	public void insert(Role role);

	public void batchInsert(List<Role> list);

	public void update(Role role);

	public void delete(String id);
}
